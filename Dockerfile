# Elixir 1.3.2. : https://hub.docker.com/_/elixir/
FROM elixir:1.3.2

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

ADD . /app

RUN mix local.hex --force

RUN mix local.rebar --force

RUN mix deps.get

# NPM NOT NEEDED FOR API ONLY

# RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

# RUN apt-get install -y -q nodejs

CMD mix ecto.create && mix ecto.migrate && mix phoenix.server
