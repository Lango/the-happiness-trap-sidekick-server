#!/usr/bin/env bash
set -e

echo '>>> Get old container id'
CID=$(docker ps -a | grep "the-happiness-trap-sidekick-server" | awk '{print $1}')
echo $CID

echo '>>> Get latest image'
docker pull registry.gitlab.com/lango/the-happiness-trap-sidekick-server:server-latest

echo '>>> Stopping old container'
if [ "$CID" != "" ];
then
  docker stop $CID
fi

echo '>>> Delete old container'
if [ "$CID" != "" ];
then
  docker rm $CID
fi

echo '>>> Remove old docker image'
docker images | grep "<none>" | head -n 1 | awk 'BEGIN { FS = "[ \t]+" } { print $3 }'  | while read -r id ; do
   docker rmi $id
done

echo '>>> Starting new container'
docker run -d  --link thts-db:db --name the-happiness-trap-sidekick-server -p 4001:4000 registry.gitlab.com/lango/the-happiness-trap-sidekick-server:server-latest

echo '>>> Sleep for 30 seconds to give time for server to start up'
sleep 30

echo '>>> ALL DONE :)'
