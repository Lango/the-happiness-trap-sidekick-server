defmodule ThtsServer.Router do
  use ThtsServer.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ThtsServer.Api do
    pipe_through :api

    get "/choices", ChoiceController, :index
  end
end
