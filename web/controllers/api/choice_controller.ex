defmodule ThtsServer.Api.ChoiceController do
  use ThtsServer.Web, :controller

  plug :action

  def index(conn, _params) do
    choices =  [
      %{
        :questionNumber => 9415,
        :choiceId =>1,
        :choiceType => :A,
        :description => "I must have good control of my feelings in order to be successful in life."
      },
      %{
        :questionNumber => 1,
        :choiceId =>2,
        :choiceType => :B,
        :description => "It is unnecessary for me to control my feelings in order to be successful in life."
      },
      %{
        :questionNumber => 2,
        :choiceId =>3,
        :choiceType => :A,
        :description => "Anxiety is bad."
      },
      %{
        :questionNumber => 2,
        :choiceId =>4,
        :choiceType => :B,
        :description => "Anxiety is neither good nor bad. It is merely an uncomfortable feeling."
      }
    ]
    render conn, choices: choices
  end

end
