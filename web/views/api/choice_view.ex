defmodule ThtsServer.Api.ChoiceView do
  use ThtsServer.Web, :view

  def render("index.json", %{ choices: choices }) do
    choices
  end
end
